FROM openjdk:21

LABEL maintainer="Esion <m17762618644@163.com>"
LABEL description="构建属于你的知识库"

WORKDIR /app
EXPOSE 5173

VOLUME /app/data

ADD knowledge-base-*.jar /app/bin/app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/bin/app.jar"]
