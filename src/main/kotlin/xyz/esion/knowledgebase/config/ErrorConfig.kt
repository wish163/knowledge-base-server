package xyz.esion.knowledgebase.config

import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import xyz.esion.knowledgebase.component.R

/**
 * @author Esion
 * @since 2024/1/23
 */
@RestControllerAdvice
class ErrorConfig {
    @ExceptionHandler(IllegalArgumentException::class)
    fun illegalArgumentException(ex: IllegalArgumentException): R<Void> {
        return R.error(ex.message ?: "")
    }
}