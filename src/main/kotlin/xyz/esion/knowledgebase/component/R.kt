package xyz.esion.knowledgebase.component

import java.io.Serializable

/**
 * @author Esion
 * @since 2024/1/22
 */
data class R<T>(val success: Int, val msg: String, val data: T?): Serializable {

     companion object {
         fun <T> success(data:T): R<T> {
             return R(200, "", data)
         }

         fun noAuth(): R<Void> {
             return R(401, "未认证", null)
         }

         fun error(msg: String): R<Void> {
             return R(500, msg, null)
         }

     }
}
