package xyz.esion.knowledgebase

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import xyz.esion.knowledgebase.properties.ConfigProperties

@SpringBootApplication
@EnableConfigurationProperties(value = [ConfigProperties::class])
@EnableMongoRepositories(basePackages = ["xyz.esion.knowledgebase.repository"])
class KnowledgeBaseApplication

fun main(args: Array<String>) {
    runApplication<KnowledgeBaseApplication>(*args)
}
