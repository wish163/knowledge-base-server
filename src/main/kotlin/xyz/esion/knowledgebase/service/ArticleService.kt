package xyz.esion.knowledgebase.service

import xyz.esion.knowledgebase.domain.ArticleInfo
import xyz.esion.knowledgebase.domain.ArticleTree

/**
 * @author Esion
 * @since 2024/1/23
 */
interface ArticleService {

    /**
     * 使用树展示文章
     *
     * @param title 可能存在的文章标题过滤
     * @return 文章树
     */
    fun tree(title: String): List<ArticleTree>

    /**
     * 根据ID获取文章信息
     */
    fun info(id: Int): ArticleInfo

}