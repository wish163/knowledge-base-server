package xyz.esion.knowledgebase.domain


/**
 * 文章树
 *
 * @author Esion
 * @since 2024/1/23
 */
data class ArticleTree(
    val key: Int,
    val title: String,
    val isLeaf: Boolean,
    var children: List<ArticleTree>
)
