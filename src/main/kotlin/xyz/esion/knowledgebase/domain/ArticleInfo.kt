package xyz.esion.knowledgebase.domain

import xyz.esion.knowledgebase.entity.article.ArticleBase
import xyz.esion.knowledgebase.entity.article.ArticleIndex
import java.time.LocalDateTime

/**
 * @author Esion
 * @since 2024/1/23
 */
data class ArticleInfo(

    /**
     * ID
     */
    val id: Int,

    /**
     * 恢复版本
     */
    val rev: String,

    /**
     * 创建时间
     */
    val createTime: LocalDateTime,

    /**
     * 更新时间
     */
    val updateTime: LocalDateTime,

    /**
     * 文章名字
     */
    val name: String,

    /**
     * 分类
     */
    val categoryId: Int,

    /**
     * 所在目录，根目录为0
     */
    val folder: Int,

    /**
     * 是否是预览模式
     */
    val preview: Boolean,

    /**
     * 文章类型
     *
     * @see xyz.esion.knowledgebase.enumeration.ArticleTypeEnum
     */
    val type: Int,

    /**
     * 文章标签
     */
    val tag: List<String>,

    /**
     * 描述，限制64个字
     */
    val description: String,

    /**
     * 来源，最多32个字
     */
    val source: String,

    /**
     * 源链接
     */
    val sourceUrl: String
) {
    constructor(index: ArticleIndex, base: ArticleBase) : this(
        index.id,
        index.rev,
        index.createTime,
        index.updateTime,
        index.name,
        index.categoryId,
        index.folder,
        index.preview,
        index.type,
        base.tag,
        base.description,
        base.source,
        base.sourceUrl
    )
}
