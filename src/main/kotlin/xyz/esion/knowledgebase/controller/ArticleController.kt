package xyz.esion.knowledgebase.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import xyz.esion.knowledgebase.component.R
import xyz.esion.knowledgebase.domain.ArticleInfo
import xyz.esion.knowledgebase.domain.ArticleTree
import xyz.esion.knowledgebase.service.ArticleService

/**
 * 文章
 *
 * @author Esion
 * @since 2024/1/23
 */
@RestController
@RequestMapping("api/article")
class ArticleController(
    private val articleService: ArticleService
) {

    /**
     * 查询文章列表
     */
    @GetMapping("tree")
    fun tree(@RequestParam(value = "name", required = false, defaultValue = "") name: String): R<List<ArticleTree>> {
        return R.success(articleService.tree(name))
    }

    /**
     * 查询文章详情
     */
    @GetMapping("info/{id}")
    fun info(@PathVariable id: Int): R<ArticleInfo> {
        return R.success(articleService.info(id))
    }

}