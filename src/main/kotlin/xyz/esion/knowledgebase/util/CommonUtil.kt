package xyz.esion.knowledgebase.util

import cn.hutool.core.collection.CollUtil

fun collStartWiths(keywords: List<String>, template: String): Boolean {
    if (CollUtil.isEmpty(keywords)) {
        return true
    }
    return keywords.any { keyword -> template.startsWith(keyword) };
}