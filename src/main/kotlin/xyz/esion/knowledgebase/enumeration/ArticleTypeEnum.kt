package xyz.esion.knowledgebase.enumeration

/**
 * 文章类型
 *
 * @author Esion
 * @since 2024/1/23
 */
enum class ArticleTypeEnum(val value: Int) {

    MARKDOWN(1),

    RICH_TEXT(4),

    CODE(3)

}