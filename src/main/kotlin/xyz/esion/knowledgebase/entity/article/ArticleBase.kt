package xyz.esion.knowledgebase.entity.article

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

/**
 * @author Esion
 * @since 2024/1/23
 */
@Document("article_base")
data class ArticleBase(

    @Id val id: Int,

    /**
     * 文章标签
     */
    val tag: List<String>,

    /**
     * 描述，限制64个字
     */
    val description: String,

    /**
     * 来源，最多32个字
     */
    val source: String,

    /**
     * 源链接
     */
    val sourceUrl: String

) {
    constructor(): this(0, ArrayList(), "", "", "")
}
