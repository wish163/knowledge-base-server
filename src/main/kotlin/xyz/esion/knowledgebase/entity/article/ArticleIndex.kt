package xyz.esion.knowledgebase.entity.article

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

/**
 * 文章索引
 *
 * @author Esion
 * @since 2024/1/23
 */
@Document("article_index")
data class ArticleIndex(

    /**
     * ID
     */
    @Id val id: Int,

    /**
     * 恢复版本
     */
    val rev: String,

    /**
     * 创建时间
     */
    val createTime: LocalDateTime,

    /**
     * 更新时间
     */
    val updateTime: LocalDateTime,

    /**
     * 文章名字
     */
    val name: String,

    /**
     * 分类
     */
    val categoryId: Int,

    /**
     * 所在目录，根目录为0
     */
    val folder: Int,

    /**
     * 是否是预览模式
     */
    val preview: Boolean,

    /**
     * 文章类型
     *
     * @see xyz.esion.knowledgebase.enumeration.ArticleTypeEnum
     */
    val type: Int,
)
