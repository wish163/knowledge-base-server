package xyz.esion.knowledgebase.entity.article

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

/**
 * @author Esion
 * @since 2024/1/23
 */
@Document("article_folder")
data class ArticleFolder(
    @Id val id: Int,
    val pid: Int,
    val name: String
)
