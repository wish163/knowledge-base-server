package xyz.esion.knowledgebase.properties

import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * @author Esion
 * @since 2024/1/22
 */
@ConfigurationProperties(prefix = "knowledge-base")
data class ConfigProperties(

    /**
     * token
     */
    val token: String

)
