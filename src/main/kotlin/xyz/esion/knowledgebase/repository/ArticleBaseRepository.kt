package xyz.esion.knowledgebase.repository

import org.springframework.data.mongodb.repository.MongoRepository
import xyz.esion.knowledgebase.entity.article.ArticleBase

/**
 * @author Esion
 * @since 2024/1/23
 */
interface ArticleBaseRepository: MongoRepository<ArticleBase, Int> {
}