package xyz.esion.knowledgebase.repository

import org.springframework.data.mongodb.repository.MongoRepository
import xyz.esion.knowledgebase.entity.article.ArticleFolder

/**
 * @author Esion
 * @since 2024/1/23
 */
interface ArticleFolderRepository: MongoRepository<ArticleFolder, Int> {
}