package xyz.esion.knowledgebase.repository

import org.springframework.data.mongodb.repository.MongoRepository
import xyz.esion.knowledgebase.entity.article.ArticleIndex

/**
 * @author Esion
 * @since 2024/1/23
 */
interface ArticleIndexRepository: MongoRepository<ArticleIndex, Int> {

    /**
     * 根据名字查询文章索引，忽略大小写
     * @param name 文章名
     * @return 文章索引
     */
    fun findByNameContainingIgnoreCase(name: String): List<ArticleIndex>

}